﻿# Thông tin cá nhân
- MSSV: 1512557
- Họ tên: Phan Trọng Thuyên
# Các chức năng đã làm được
## Vẽ 5 loại hình cơ bản

1. Đường thẳng (line). Dùng hàm MoveToEx và LineTo.
2. Hình chữ nhật (rectangle). Dùng hàm Rectangle. Nếu giữ phím Shift sẽ vẽ hình vuông (Square)
3. Hình Ellipse. Dùng hàm Ellipse. Nếu giữ phím Shift sẽ vẽ hình tròn (Circle)
- Cho phép chọn loại hình cần vẽ từ menu
- Thể hiện đang chọn menu nào, menu được chọn sẽ có dấu check

## Yêu cầu nâng cao
1. 
- Bọc tất cả các đối tượng vẽ vào các lớp model
- Sử dụng đa xạ (polymorphism) để cài đặt việc quản lý các đối tượng và vẽ hình.
- Sử dụng mẫu thiết kế prototypes để tạo ra hàng mẫu nhằm vẽ ở chế độ xem trước (preview).
2. Lưu và nạp hình

## Các luồng sự kiện chính
> Chạy chương trình, mặc định nét vẽ là nét liền, màu đen, chưa định vị màu tô
> Loại hình vẽ mặc định là đường thẳng
> Nhấn giữ chuột trái và di chuyển con trỏ chuột đến vị trí kết thúc, nhả chuột thì hình vẽ tương ứng tạo ra
> Trong quá trình di chuyển thì hình vẽ được preview
> Menu Draw cho phép lựa chọn loại hình
> Menu File cho phép tạo mới, mở file bitmap, lưu file bitmap

## Các luồng sự kiện phụ
> Vẽ không nháy
> Hình không đè lên nhau

## Yêu cầu khác
> Link repo: git@bitbucket.org:billhcmus/simplepaint.git

> Link video: https://www.youtube.com/watch?v=xRnRmAnCq80

> Nền tảng build: Visual Studio 2017