#include "stdafx.h"
#include "CRectangle.h"


CRectangle::CRectangle()
{
}

CRectangle::CRectangle(POINT startpos, POINT endpos) : CShape(startpos, endpos)
{

}

CRectangle::~CRectangle()
{
}

void CRectangle::Draw(HDC hdc)
{
	SelectObject(hdc, hBrush);
	Rectangle(hdc, this->StartPos.x, this->StartPos.y, this->EndPos.x, this->EndPos.y);
}