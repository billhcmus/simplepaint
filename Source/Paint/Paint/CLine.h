#pragma once
#include "Shape.h"
class CLine : public CShape
{
public:
	CLine();
	CLine(POINT, POINT);
	~CLine();
	virtual void Draw(HDC);
	CShape* Clone()
	{
		return new CLine;
	}
};

