#pragma once
#include "Shape.h"
class CRectangle : public CShape
{
public:
	CRectangle();
	CRectangle(POINT, POINT);
	virtual void Draw(HDC);
	CShape* Clone()
	{
		return new CRectangle;
	}
	~CRectangle();
};

