#include "stdafx.h"
#include "CBMP.h"


CBMP::CBMP()
{
}


CBMP::~CBMP()
{
}

CBMP::CBMP(HWND hWnd, LPTSTR path)
{
	HBITMAP hbm;
	HPALETTE hPalette;
	LoadBitmapFromBMPFile(path, &hbm, &hPalette);
}

void CBMP::Draw(HDC hdcMem)
{
	SelectObject(hdcMem, this->hBrush);
	SelectObject(hdcMem, this->hPen);
	Rectangle(hdcMem, this->StartPos.x, this->StartPos.y, this->EndPos.x, this->EndPos.y);
}


BOOL CBMP::LoadBitmapFromBMPFile(LPTSTR szFileName, HBITMAP *phBitmap,
	HPALETTE *phPalette)
{

	BITMAP  bm;

	*phBitmap = NULL;
	*phPalette = NULL;

	// Use LoadImage() to get the image loaded into a DIBSection
	*phBitmap = (HBITMAP)LoadImage(NULL, szFileName, IMAGE_BITMAP, 0, 0,
		LR_CREATEDIBSECTION | LR_DEFAULTSIZE | LR_LOADFROMFILE);
	if (*phBitmap == NULL)
		return FALSE;

	// Get the color depth of the DIBSection
	GetObject(*phBitmap, sizeof(BITMAP), &bm);
	StartPos.x = StartPos.y = 0;
	EndPos.x = bm.bmWidth;
	EndPos.y = bm.bmHeight;
	this->hPen = CreatePen(NULL_PEN, 0, 0);
	this->hBrush = CreatePatternBrush(*phBitmap);
	// If the DIBSection is 256 color or less, it has a color table
	if ((bm.bmBitsPixel * bm.bmPlanes) <= 8)
	{
		HDC           hMemDC;
		HBITMAP       hOldBitmap;
		RGBQUAD       rgb[256];
		LPLOGPALETTE  pLogPal;
		WORD          i;

		// Create a memory DC and select the DIBSection into it
		hMemDC = CreateCompatibleDC(NULL);
		hOldBitmap = (HBITMAP)SelectObject(hMemDC, *phBitmap);
		// Get the DIBSection's color table
		GetDIBColorTable(hMemDC, 0, 256, rgb);
		// Create a palette from the color tabl
		pLogPal = (LOGPALETTE *)malloc(sizeof(LOGPALETTE) + (256 * sizeof(PALETTEENTRY)));
		pLogPal->palVersion = 0x300;
		pLogPal->palNumEntries = 256;
		for (i = 0; i<256; i++)
		{
			pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
			pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
			pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
			pLogPal->palPalEntry[i].peFlags = 0;
		}
		*phPalette = CreatePalette(pLogPal);
		// Clean up
		free(pLogPal);
		SelectObject(hMemDC, hOldBitmap);
		DeleteDC(hMemDC);
	}
	else   // It has no color table, so use a halftone palette
	{
		HDC    hRefDC;

		hRefDC = GetDC(NULL);
		*phPalette = CreateHalftonePalette(hRefDC);
		ReleaseDC(NULL, hRefDC);
	}
	return TRUE;

}