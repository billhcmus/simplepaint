#pragma once
#include "Shape.h"
class CEllipse : public CShape
{
public:
	CEllipse();
	CEllipse(POINT, POINT);
	~CEllipse();
	virtual void Draw(HDC);
	CShape* Clone()
	{
		return new CEllipse;
	}
};

