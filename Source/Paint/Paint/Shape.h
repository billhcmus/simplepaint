#pragma once
#include <Windows.h>

class CShape
{
protected:
	POINT StartPos, EndPos;
	HBRUSH hBrush;
public:
	void SetStartPos(POINT);
	void SetEndPos(POINT);
	void SetBrush(HBRUSH);
	CShape();
	CShape(POINT, POINT);
	virtual void Draw(HDC) = 0;
	virtual CShape* Clone() = 0;
};