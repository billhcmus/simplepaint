#include "stdafx.h"
#include "Shape.h"

CShape::CShape()
{

}

CShape::CShape(POINT startpos, POINT endpos)
{
	this->StartPos = startpos;
	this->EndPos = endpos;
}

void CShape::SetStartPos(POINT startpos)
{
	this->StartPos = startpos;
}

void CShape::SetEndPos(POINT endpos)
{
	this->EndPos = endpos;
}

void CShape::SetBrush(HBRUSH hbr)
{
	this->hBrush = hbr;
}