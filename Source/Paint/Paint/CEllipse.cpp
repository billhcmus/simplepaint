#include "stdafx.h"
#include "CEllipse.h"


CEllipse::CEllipse()
{
}


CEllipse::~CEllipse()
{
}

CEllipse::CEllipse(POINT startpos, POINT endpos) : CShape(startpos, endpos)
{

}

void CEllipse::Draw(HDC hdc)
{
	SelectObject(hdc, this->hBrush);
	Ellipse(hdc, this->StartPos.x, this->StartPos.y, this->EndPos.x, this->EndPos.y);
}