#pragma once
#include "Shape.h"
class CBMP :
	public CShape
{
private:
	HPEN hPen;
public:
	BOOL LoadBitmapFromBMPFile(LPTSTR szFileName, HBITMAP *phBitmap,
		HPALETTE *phPalette);
	void Draw(HDC);
	CShape* Clone()
	{
		return NULL;
	}
	CBMP();
	CBMP(HWND hWnd, LPTSTR path);
	~CBMP();
};

