#include "stdafx.h"
#include "CLine.h"


CLine::CLine()
{
}

CLine::CLine(POINT startpos, POINT endpos) : CShape(startpos, endpos)
{

}


CLine::~CLine()
{
}


void CLine::Draw(HDC hdc)
{
	SelectObject(hdc, this->hBrush);
	MoveToEx(hdc, this->StartPos.x, this->StartPos.y, NULL);
	LineTo(hdc, this->EndPos.x, this->EndPos.y);
}